import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入 view-design 组件
import ViewUI from 'view-design';
// 引入 view-design 组件样式
import 'view-design/dist/styles/iview.css';

// 按需引入 
// import { Popup } from 'vant'
// 按需引入 popup的 css样式
// import 'vant/lib/popup/index.css'

// import Vant from 'vant';
import 'vant/lib/index.css';

// 引入stroe 
import store from './store';

Vue.config.productionTip = false

// 在Vue中使用view-ui-plus组件
Vue.use(ViewUI)
// 使用 vant组件
// Vue.use(Vant);

new Vue({
  router,
  // 在vue实例中 使用store
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
