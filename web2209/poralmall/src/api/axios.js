// 配置axios文件
// 引入module的 axios
import axios from "axios";
import iview from 'view-design'
// 导入store
import store from '@/store'
// 1.创建axios
const Axios = axios.create({
    // 请求的前缀
    // baseURL: 'http://www.poralmallcors.com/',
    baseURL: 'http://192.168.17.126/',
    // 请求超时的时间 5000ms
    timeout: 5000
})

// 在axios添加拦截器interceptors
Axios.interceptors.request.use(config => {
    // config 拦截器配置参数
    config.headers.token = store.state.token;
    // 把配置好的信息返回出去，不反悔则 请求不会进行
    return config
}, err => {
    // err 是请求错误的 信息
    Promise.reject(err)
})

// 处理请求的状态码
Axios.interceptors.response.use(res => {
    // 我们一般在这里处理，请求成功后的错误状态码 例如状态码是500，404，403
    // res 是所有相应的信息
    if(res.status != 200){
        // message  服务器给我们返回的信息
        // 如果请求的状态码不是200 证明请求失败
        iview.Message.error(res.message)
    }
   return Promise.resolve(res)
}, err => {
    // 请求出现问题 才会提示的回调函数
    // console.log(iview);
    if(err.status != 200){
        // message  服务器给我们返回的信息
        iview.Message.error(err.message)
    }
    // 服务器响应发生错误时的处理
    Promise.reject(err)
})
// 导出Axios
export default Axios