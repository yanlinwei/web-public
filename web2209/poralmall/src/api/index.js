// 接口信息的文件

// 1.引入配置好的axios文件
import api from './axios';

// 导出获取验证码的接口 
export const getCode = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('getCode',data)
}

// 导出用户注册的接口
export const register = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('register',data)
}

// 导出用户登录的接口
export const login = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('login',data)
}

// 导出获取用户信息的接口
export const getUserInfo = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('getUserInfo',data)
}

// 导出上传文件接口
export const uploadFile = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('upload',data)
}

// 导出修改资料接口
export const editUserInfo = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('editUserInfo',data)
}

// 导出修改密码接口
export const forgetPassword = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('forgetPassword',data)
}

// 导出投诉建议接口
export const advise = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('advise',data)
}

// 导出评分接口
export const giveStar = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('giveStar',data)
}

// 导出退出登录接口
export const loginOut = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('loginOut',data)
}

// 导出极豆明细接口
export const getPolarList = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('getPolarList',data)
}

// 导出赚极豆接口
export const getPolar = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('getPolar',data)
}

// 导出收货地址接口
export const getAddressList = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('getAddressList',data)
}

// 修改收货地址接口
export const editAddress = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('editAddress',data)
}

// 新增收货地址接口
export const addAddress = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('addAddress',data)
}

// 获取收货地址详情接口
export const getAddressDesc = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('getAddressDesc',data)
}

// 删除收货地址接口
export const deleteAddressDesc = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('deleteAddressDesc',data)
}

// 获取首页数据
export const getHome = (params) => {
    return api.get('getHome',{
        params
    })
}

// 获取商品详情数据
export const getProductDesc = (params) => {
    return api.get('getProductDesc',{
        params
    })
}

// 加入购物车接口
export const addCartList = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('addCartList',data)
}

// 获取购物车列表
export const getCartList = (params) => {
    return api.get('getCartList',{
        params
    })
}

// 删除购物车接口
export const delCartNum = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('delCartNum',data)
}

// 删除购物车接口
export const editCartNum = (data) => {
    // 把axios post请求信息 直接返回出去
    return api.post('editCartNum',data)
}