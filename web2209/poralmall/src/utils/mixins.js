var mixin = {
    // 在这里面可以写 vue的任何方法，
    // mixins 就相当于 另外一个 vue的实例
    /* 
        mixins混入，就相当于要比使用在哪个实例/组件中的生命周期
        要快，
        在被使用的实例周期之前发生，
        如果mixins和实例存在相同的方法，或者生命周期
        mixins的方法和生命周期会线性启动;
        作用：
            多用于，公共方法多一些;

            如果有公用的数据可以使用vueX的状态管理
    */
    // created() {
    //     console.log(123);
    // },
    data(){
        return {
            msg: 'hello world'
        }
    },
    methods: {
        hello(){
            alert('hello World')
        }
    }
}


export default mixin