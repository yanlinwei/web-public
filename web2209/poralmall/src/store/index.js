/* 
    vueX  状态的管理模式:
        最初设计vuex是为了，让每个组件的通用数据，
        有一个可以同意管理的的情况，而出现的;
        不再每个组件的生命周期当中的 data中，写入重复的数据而出现的;

        再后来，衍生出了前端可以去调试任何的前端项目;
        但是，依然不能够将调试好的数据，存储在本地环境当中;
        购物车，增删改，在前端调试的过程当中，可以实现页面上的改动;
        但是重新启动项目或者，打开页面的时候，所有的数据又会重新回到
        初始的页面上;
*/

// 引入vue
import Vue from 'vue';
// 引入vuex
import VueX from 'vuex';
// 在vue中使用vuex
Vue.use(VueX);

// 创建store 实例
const store = new VueX.Store({
    state: {
        /* 
            state 所有的共享数据
            都要放在state里面
            和组件的data函数类似
            state的状态是 双向绑定的
            改变了state的状态，会通知给被赋值的一方;
        */
        token: localStorage.getItem('token'),
        pro_list:[
            {
                name: '天线宝宝',
                price: '0.01',
                hot: 9999,
                num: 456
            },
            {
                name: '天线宝宝',
                price: '0.01',
                hot: 9999,
                num: 456
            },
            {
                name: '天线宝宝',
                price: '0.01',
                hot: 9999,
                num: 456
            }
        ]
    },
    actions:{
        /* 
            可以调用 mutations里面的方法

        */

        ListChange(context,index){
            /* 
                context 是 mutations的对象
                通过context调用 mutations的方法
                里面可以 进行异步更改

                把 mutations里面的方法 分发给了 actions

            */
           setTimeout(() => {
            context.commit('proListChange',index)
           }, 1500);
        }
    },
    mutations:{
        // state专属的方法
        proListChange(state,index){
            // state 必选参数 传递的值 是 state的状态
            // index 触发当前函数 传递的值
            // console.log(state,index);
            state.pro_list.splice(index,1)
        },
        tokenChange(state,token){
            state.token = token;
        }
    },
    // store的计算属性
    getters: {
        count: state => {
            let num = 0;
            state.pro_list.forEach(item => {
                num += item.hot;
            })
            return num;
        }
    },
    modules:{
        /* 
            单一状态树:
            里面的对象，相当于一个小的 store
            里面的属性 都包含了 
            state getters mutations actions modules
        */
       abc: {
        state: {

        },
        getters:{

        },
        mutations:{

        },
        actions: {

        },
        modules:{

        }
       }
    }
})

// 导出store
export default store