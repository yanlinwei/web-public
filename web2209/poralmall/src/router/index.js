import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    // 首页的导航视图
    path: '/',
    component: () => import('@/views/tabView'),
    // 添加子路由视图
    children:[
      {
        path: '/',
        alias: '/home',
        component: () => import('@/views/home')
      },
      {
        path: '/community',
        alias: '/community',
        component: () => import('@/views/community')
      },
      {
        path: '/cart',
        alias: '/cart',
        component: () => import('@/views/cart')
      },
      {
        path: '/myself',
        alias: '/myself',
        component: () => import('@/views/myself'),
        // meta 自定义属性
        meta: {
          // isLogin  判当前页面是否需要登录
          isLogin: true
        }
      }
    ]
  },
  {
    // 登录
    path: '/Login',
    name: 'Login',
    component: () => import('@/views/Login')
  },
  {
    // 注册
    path: '/Register',
    name: 'Register',
    component: () => import('@/views/Register')
  },
  {
    // 设置
    path: '/setUp',
    component: () => import('@/views/myself/setUp'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 修改资料
    path: '/Edit',
    component: () => import('@/views/myself/setUp/editUserInfo'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 修改密码
    path: '/Forget',
    component: () => import('@/views/myself/setUp/forget'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 投诉建议
    path: '/Suggest',
    component: () => import('@/views/myself/setUp/advise'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 评分
    path: '/Star',
    component: () => import('@/views/myself/setUp/star'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 极豆明细
    path: '/PolarList',
    component: () => import('@/views/myself/polarList'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 极豆明细
    path: '/Task',
    component: () => import('@/views/myself/task'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 收货地址
    path: '/Address',
    component: () => import('@/views/myself/address'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 新增收货地址
    path: '/Address/Add',
    component: () => import('@/views/myself/address/add'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 编辑收货地址
    // 动态路由
    path: '/Address/Edit/:id',
    component: () => import('@/views/myself/address/edit'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  },
  {
    // 商品详情
    path: '/product/detail',
    component: () => import('@/views/product/detail'),
    // meta 自定义属性
    meta: {
      // isLogin  判当前页面是否需要登录
      isLogin: true
    }
  }
]


// 路由守卫
const router = new VueRouter({
  routes
})

// 引入vuex
import store from '@/store'
// 路由前置守卫
router.beforeEach((to,from,next) => {
  // console.log(to);
  // console.log(from);
  // 再路由跳转之前会先进行此函数
  /* 
    to 往哪里去
      包含了 跳转后的路由信息
    from 从哪里来
      包含了 跳转前的路由信息
    next 去干嘛
      一个行进函数
      默认不填写值
      next('要跳转的路由信息')
  */
//  存在token
 if(store.state.token != '' && store.state.token != null){
  next()
 }else{
  // 如果不存在token并且 要跳转的路由 必须登录
  if(to.meta.isLogin == true){
    next('/Login')
  }else{
    next()
  }
 }
//  next()
})

export default router
