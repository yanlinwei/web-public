import axios from 'axios';
import { ElMessage } from 'element-plus'
// 创建axiox
const Axios = axios.create({
    // 请求前缀
    baseURL: 'https://admin-api.macrozheng.com/',
    // 超时时间 5000ms
    timeout: 5000
})

// 发起请求前 做的事情  给axios添加请求拦截
Axios.interceptors.request.use(config => {
    // 添加 请求头 token
    config.headers.Authorization = sessionStorage.getItem('token');
    // 把配置返回出去,如果返回 请求不会进行
    return config
})

// 请求后拦截状态
// 处理请求的状态码
Axios.interceptors.response.use(res => {
    // 我们一般在这里处理，请求成功后的错误状态码 例如状态码是500，404，403
    // res 是所有相应的信息
    if(res.status != 200){
        // message  服务器给我们返回的信息
        // 如果请求的状态码不是200 证明请求失败
        ElMessage.error(res.message)
    }
   return Promise.resolve(res)
}, err => {
    // 请求出现问题 才会提示的回调函数
    // 服务器响应发生错误时的处理
    ElMessage.error(err.message)
    Promise.reject(err)
})
export default Axios