import api from './axios';

// 登录接口
export const Login = (data) => api.post('admin/login',data)

// 获取菜单接口
export const getMenus = (data) => api.get('admin/info',data)

// 获取用户列表接口
export const getAdminList = (params) => api.get('admin/list',{params})

// 获取当前用户的角色列表
export const getRole = (id) => api.get('admin/role/' + id)

// 获取所有角色列表
export const getRoleList = () => api.get('role/listAll')

// 给用户分权限
export const roleUpdate = (data) => api.post('admin/role/update',data)

// 删除指定管理员
export const delAdmin = (id) => api.post('admin/delete/' + id)

// 编辑指定管理员
export const editAdminRole = (data) => api.post('admin/update/'+data.id, data)

// 添加管理员
export const addAdminRole = (data) => api.post('admin/register',data)

// 获取角色列表
export const getListRole = (params) => api.get('role/list',{params})

// 添加角色
export const createRole = (data) => api.post('role/create',data)

// 修改角色信息
export const updateRole = (data) => api.post('role/update/' + data.id,data)

// 删除角色
export const deleteRole = (data) => api.post('role/delete',data)

// 获取菜单列表
export const treeList = () => api.get('menu/treeList');

// 获取当前用户拥有的 菜单列表
export const roleTreeList = (id) => api.get('role/listMenu/' + id)

// 给角色分配菜单
export const allocMenuRole = (data) => api.post('role/allocMenu',data)


// 获取商品品牌列表
export const getBrand = (params) => api.get('brand/list', {params})

// 获取商品分类接口
export const withChildren = () => api.get('productCategory/list/withChildren')

// 获取商品的列表
export const porductList = (params) => api.get('product/list',{params})

// 获取商品详情
export const productInfo = (id) => api.get('product/updateInfo/' + id)

// 获取商品属性分类列表
export const atrrCategoryList = (id) => api.get('productAttribute/category/list', {
    params: {
        pageNum: 1,
        pageSize: 100
    }
})

// 获取商品的规格信息
export const buteList = (id) => api.get('productAttribute/list/'+id, {
    params: {
        pageNum:1,
        pageSize:100,
        type: 0
    }
})