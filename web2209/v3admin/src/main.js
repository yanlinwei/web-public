import { createApp } from 'vue'
import App from './App.vue'

import './index.css'
const app = createApp(App);
// 引入router路由
import router from './router'
// 全局使用路由
app.use(router);
// 引入element plus 组件
import ElementPlus from 'element-plus'
// 引入element plus 组件样式
import 'element-plus/dist/index.css'
// 使用element plus
app.use(ElementPlus)
// 引入element plus 图标组件
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// 引用所有的图标组件
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

// 引入图标
import './font_/iconfont.css'

app.mount('#app')
