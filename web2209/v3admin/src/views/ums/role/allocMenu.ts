/* 
    js语言的是弱语言，
        因为在数据判断时，不需要给数据定义必须是什么类型的值
        但是ts 让我们的js变成了强类型的数据语言;
    
        强类型语言:
            Java,C,C#,.net,Go....
*/


// 定义ts数据
// 声明(let/var/const) 变量名: 数据类型  = 值;

// 定义基本数据类型

export const isBool:boolean = true;

export const isNumber:number = 0;

export const isString:string = '0';

export const isNull:null = null;

export const isUndef:undefined = undefined;

// 定义引用数据类型 Array 和  Object
// 定义数值类型的数组，数组里面只能是数值类型的数据
// export const isArray:number[] = [0,1,2,3]
// export const isArray:Array<number> = [0,1,2,3]

// 元组  用于匹配 我们知道数组有多少个 长度 并且每一个值 都要定义对应的数据类型

export const isArray: [string,number,object] = ['1',0,{}]


// 不知道我们要获取的值是什么数据类型  我们可以使用any
/* 
    在ts里面，如果不知道 数据类型要定义成什么数据
    可以使用any告诉 ts  它可以是任何类型的值
*/

// vue3 + ts
export const isAny:any = undefined;

// void
/* 
    void 什么类型都不是，
    undefined
    也可以在没有返回值的 函数定义
*/
export const isVoid:void = undefined;

function isVoids():void{
    return undefined
}
