// 引入vue4 的 创建路由函数
import {createRouter, createWebHashHistory} from 'vue-router';

let routes = [
    {
        path: '/',
        // 模板
        component: () => import('@/views/tabView/index.vue'),
        meta: {
            isLogin: true
        },
        children: [{
            path:'/',
            name: 'home',
            component: () => import('@/views/home/index.vue')
        }]
    },
    {
        path: '/login',
        component: () => import('@/views/Login/index.vue')
    },
    {
        // 用户模块
        path:'/ums',
        // 根据哪个模板 渲染子路由
        component: () => import('@/views/tabView/index.vue'),
        children: [{
            path: 'admin',
            name: 'admin',
            component: () => import('@/views/ums/admin.vue'),
            meta: {
                isLogin: true
            }
        },{
            path: 'role',
            name: 'role',
            component: () => import('@/views/ums/role/index.vue'),
            meta: {
                isLogin: true
            }
        },{
            path: 'allocMenu/:roleId',
            name: 'allocMenu',
            component: () => import('@/views/ums/role/allocMenu.vue'),
            meta: {
                isLogin: true
            }
        }]
    },
    {
        // 用户模块
        path:'/pms',
        // 根据哪个模板 渲染子路由
        component: () => import('@/views/tabView/index.vue'),
        children: [{
            path: 'product',
            name: 'product',
            component: () => import('@/views/pms/product.vue'),
            meta: {
                isLogin: true
            }
        },{
            path: 'updateProduct/:id',
            name: 'updateProduct',
            component: () => import('@/views/pms/update/detail.vue'),
            meta: {
                isLogin: true
            }
        }]
    }
]

const router = new createRouter({
    // 创建hash路由
    history:createWebHashHistory(),
    routes
})

router.beforeEach((to, from, next) => {
    if(
        (sessionStorage.getItem('token') == '' || sessionStorage.getItem('token') == null)
        && to.meta.isLogin == true
    ){
        // 如果没有登录  滚去登录页面
        next('/login')
    }else{
        // 登录了 想去哪去哪
        next();
    }
})

export default router