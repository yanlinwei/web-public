import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  // 打包项目的时候 里面的条件

  /* 
    所谓 生产环境和 开发环境的区别
      开发环境，指的是我们在开发项目的时候，
      所运用的电脑的端配置环境
      这个环境里面，包含 node_modules,src,static,assets,index.html  等配置文件;

      生产环境,就是在服务器运行我们写好的项目代码.
      无论是vue项目还是react还是uni-app还是小程序都需要打包后上传到
      服务器;
      让服务器运行我们的打包后的代码。
      对于我们前端来讲，打包好的dist文件，在没有配置生产环境的情况下
      是打不开我们打包好的文件的;
      不过有个好处，
        服务器呢，是后端配置的，我们只需要把打包好的文件，发给后端，或者让后端开一个ftp
        账号，我们自己上传到服务器，就可以直接访问我们的项目;
  */
  plugins: [
    // 这个是 vue文件
    vue(),
  ],
  resolve: {
    // 配置路径别名
    alias: {
      '@': path.resolve(__dirname, 'src'),
      'views': '@/views',
      'assets': '@/assets',
      'common': '@/common',
      'components': '@components',
      'network': '@/network',
      'router': '@/router',
      'store': '@/store',
      "api": "@/api"
    },
    // 省略文件后缀
    extensions: ['', '.js', '.json', '.vue', '.scss', '.css', '.ts', '.tsx']
  },
  server: {
    // 修改端口
    port: 8888,
    // 本机的ip地址 一般来说 不需要进行更改
    host:'0.0.0.0',
    // 服务启动完成后 是否自动打开浏览器 运行这个项目
    open: true,
    // 配置 反向代理 一般用来 解决跨域问题
    // proxy: {
    //   '/szapi': {
    //     target: 'https://tradesz.test.api.qywgpo.com/',
    //     changeOrigin: true,
    //     rewrite: (path) => path.replace(/^\/szapi/, ''),
    //   },
    // }
    
  },
  // 配置打包环境  这个是 固定写法
  build: {
    //打包环境移除console.log，debugger
    minify: 'terser',
    terserOptions: {
      compress: {
        // 控制台打印
        drop_console: true,
        // 代码断点调试
        drop_debugger: true,
      },
    },
    //打包文件按照类型分文件夹显示
    rollupOptions: {
      input: {
        main: path.resolve(__dirname, 'index.html'),
      },
      // 打包后的代码 文件分文件夹
      output: {
        chunkFileNames: 'js/[name]-[hash].js',
        entryFileNames: 'js/[name]-[hash].js',
        assetFileNames: '[ext]/[name]-[hash].[ext]',
      },
    },
  }
})