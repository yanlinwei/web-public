// 组合式API的引入方式
import { createApp } from 'vue'

// 选项式API的引入方式
// import Vue from 'vue'
import App from './App.vue'
// 引入了一个index.css  这个
// css就是 可以在vue全局使用的样式
// import './index.css'
// 只要是在main.js里面使用的样式,和组件 都不需要在单文件vue中再次引入
createApp(App).mount('#app')
