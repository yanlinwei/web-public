import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    /* 
      如果没有指定文件夹下的某个文件，默认是index文件
    */
    component: () => import('@/views/home'),
    // 别名
    alias: '/home'
  },
  {
    path: '/category',
    /* 
      如果没有指定文件夹下的某个文件，默认是index文件
    */
    component: () => import('@/views/category')
  },
  {
    path: '/star',
    /* 
      如果没有指定文件夹下的某个文件，默认是index文件
    */
    component: () => import('@/views/star')
  },
  {
    path: '/cart',
    /* 
      如果没有指定文件夹下的某个文件，默认是index文件
    */
    component: () => import('@/views/cart')
  },
  {
    path: '/myselef',
    /* 
      如果没有指定文件夹下的某个文件，默认是index文件
    */
    component: () => import('@/views/myselef')
  }
]

const router = new VueRouter({
  routes
})

export default router
