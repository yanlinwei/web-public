// 引入vue依赖
import Vue from 'vue'
// 引入根组件 App.vue
import App from './App.vue'
// 引入路由文件
import router from './router'

// 关闭开发模式下提示
Vue.config.productionTip = false

// vue实例
new Vue({
  // 引入router
  router,
  // vue的render函数
  render: function (h) { 
    /* 
      render函数的 h参数 
      是一个函数;
      是VUE实例自带的，转换组件的方式
    */
    return h(App) 
  }
}).$mount('#app')
// $mount('#app') 挂载DOM
