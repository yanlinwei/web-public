// 引入 vue依赖
import Vue from 'vue'
// 引入 vue-router依赖
import VueRouter from 'vue-router'
// 引入news组件
// import newsView from '../views/news.vue'
/* 
  关于@符号的解释;
  一般默认main.js的文件夹开始的
*/
// import newsView from '@/views/news.vue'
// 引入sports 组件
// import sportsView from '@/views/sports.vue'
// 引入plays 组件
import playsView from '@/views/plays.vue'
// 让Vue实例 使用 VueRouter 依赖
Vue.use(VueRouter)

const routes = [
  // 配置路由
  {
    // /代表着首个显示的组件
    path: '/',
    // 命名视图
    name: 'news',
    // 引入组件视图
    // component: newsView
    component: () => import('@/views/news.vue'),
    // 配置子路由
    children: [
      {
        path: '/',
        // 引入子路由文件
        component: () => import('@/views/newsContent/hangzhou.vue'),
        // 给路由起一个别名 访问地址为/hangzhou的路由时 显示的组件仍然是hangzhou.vue
        alias: '/hangzhou'
      },
      {
        // 可以不加/
        path: 'hospital',
        // 引入子路由文件
        component: () => import('@/views/newsContent/hospital.vue')
      },
      {
        // 可以不加/
        path: 'xinping',
        // 引入子路由文件
        component: () => import('@/views/newsContent/xinping.vue')
      },{
        path: 'xian',
        component: () => import('@/views/newsContent/xinping.vue'),
        /* 
          路由的重定向，
          当跳转当前的路由，访问到的是，重定向的地址
        */
        redirect: '/xinping'
      }
    ]
  },
  {
    // /代表着首个显示的组件
    path: '/sports',
    // 命名视图
    name: 'sports',
    // 引入组件视图
    /* 
      前端开发，两大复杂点：
        一个写css，
        一个起名字
      使用import()的回调函数
    */
    // component: sportsView
    component: () => import('@/views/sports.vue')
  },
  {
    // /代表着首个显示的组件
    path: '/plays',
    // 命名视图
    name: 'plays',
    // 引入组件视图
    component: playsView
  }
]

// 挂载VueRouter实例
const router = new VueRouter({
  routes
  // 相当于 routes: routes
})

// 导出router
export default router
