// vue 3.0 导出createApp 创建vue实例函数
import { createApp } from 'vue'
// 根组件
import App from './App.vue'

// 使用创建函数 绑定 相应的根元素
createApp(App).mount('#app')
