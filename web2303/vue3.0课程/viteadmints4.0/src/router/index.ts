// 导出创建路由实例
import { createRouter,createWebHashHistory,RouteRecordRaw } from "vue-router";
// 把store 导入
import { store } from "@/store/index";
// 导入进度条插件
import NProgress from 'nprogress';
// RouteRecordRaw 是 vue-router的路由信息ts 检测语法
// 创建路由信息
const routes:Array<RouteRecordRaw> = [
    {
        // 地址
        path: '/',
        // 名称
        name: 'templateVue',
        // 路由的组件
        component: () => import('@/views/templateVue/index.vue'),
        // 重定向
        redirect: '/home',
        // 路由元信息
        meta: {
            // 模板需要 登录
            isLogin: true,
            title: null
        },
        // 子路由
        children: [
            {
                path: '/home',
                name: 'home',
                component: () => import('@/views/home/index.vue'),
                meta: {
                    isLogin: true,
                    title: '首页'
                }
            },
            {
                path: '/pms/product',
                name: 'product',
                component: () => import('@/views/pms/product.vue'),
                meta: {
                    isLogin: true,
                    title: '商品列表'
                }
            },
            {
                path: '/ums/admin',
                name: 'admin',
                component: () => import('@/views/ums/admin/index.vue'),
                meta: {
                    isLogin: true,
                    title: '用户列表'
                }
            }
        ]
    },
    {
       // 地址
       path: '/login',
       // 名称
       name: 'login',
       // 路由的组件
       component: () => import('@/views/login.vue'),
       // 路由元信息
       meta: {
           isLogin: false,
           title: '登录'
       } 
    },
    {
        // 匹配所有没有注册的路由 都要跳转当前的页面 只能匹配使用path跳转的
        path: '/:pathMatch(.*)*',
        name: 'notfound',
        component: () => import('@/views/notfound/index.vue'),
        meta: {
            isLogin: false,
            title: 'Not Found'
        }
    }
]

// 创建路由实例
const router = createRouter({
    routes,
    history: createWebHashHistory()
})

// 前置路由守卫
router.beforeEach((to,from) => {
    // 开启进度条
    NProgress.start();
    // vue-router 4 没有第三个参数next
    // 判断要去的路由是否需要登录
    if(to.meta.isLogin){
        // 用户是否已经登录
        if(store.token === null){
            // 返回一个对象 代表重定向去往的页面
            return {
                path: '/login'
            }
        }
    }
})

// 导航的后置守卫
router.afterEach((to,from) => {
    NProgress.done()
})

// 导出router实例
export default router
