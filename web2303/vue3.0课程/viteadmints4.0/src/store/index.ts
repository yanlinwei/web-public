// 引入pinia的 定义数据方式 创建pinia实例 
import { defineStore ,createPinia } from "pinia";
// 创建 pinia实例
const pinia = createPinia();
export const useStore = defineStore('main',{
    // 状态库
    state: () => {
        return {
            // token 用来判断 登录状态的
            token: null
        }
    }
});

// 导出store
export const store = useStore(pinia)
