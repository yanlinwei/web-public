export interface LoginApi {
    username: string,
    password: string
}