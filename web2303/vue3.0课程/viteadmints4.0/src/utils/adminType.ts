export interface adminListData {
    pageNum: number,
    pageSize: number,
    keyword?: string
}

export interface adminListDataType {
    id?: number,
    createTime?: null|string,
    email?: null|string,
    icon?: null|string,
    loginTime?: null|string,
    nickName?: null|string,
    note?: null|string,
    password?: null|string,
    status?: number,
    username?: null|string
}

export interface updateStatusData {
    id: number | undefined,
    status: number | undefined
}

export interface roleListData {
    adminCount: number,
    createTime: string,
    description: string,
    id: number,
    name: string,
    sort: number,
    status: number
}

export interface roleDetailData {
    email: string,
    id?:null | number,
    nickName: string,
    note: string,
    password: string,
    status: number,
    username: string
}