// 引入配置好的axios
import request from './axios';
// 引入接口的ts
import { LoginApi } from '@/utils/types';
// 创建登录接口
export const Login = (data:LoginApi) => request.post('admin/login',data)
// 创建获取菜单接口
export const Info = () => request.get('admin/info')