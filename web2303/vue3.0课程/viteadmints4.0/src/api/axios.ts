// 引入axios
import axios from 'axios';
// 引入store 
import { store } from '@/store'
// 创建axios
const instance = axios.create({
    baseURL: 'https://admin-api.macrozheng.com/',
    timeout: 5000
})

// 请求拦截
instance.interceptors.request.use((config) => {
    // 添加请求头
    config.headers.Authorization = store.token;
    return config
})

// 响应拦截
instance.interceptors.response.use((data) => {
    // 判断 ajax的状态码是否为 200
    if(data.status === 200){
        // 返回里面的数据
        return data.data
    }else{
        return data
    }
})

// 导出
export default instance