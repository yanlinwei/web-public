// 引入配置好的axios
import request from './axios';
// 导出
import { adminListData,updateStatusData,roleDetailData,adminListDataType } from '@/utils/adminType'
// 创建获取用户列表接口
export const adminList = (params:adminListData) => request.get('admin/list', {params})
// 创建删除用户接口
export const adminDelete = (id:number|undefined) => request.post('admin/delete/'+id)
// 创建修改用户是否启用的状态 
export const adminUpdateStatus = (data:updateStatusData) => request.post(`admin/updateStatus/${data.id}?status=${data.status}`)
// 创建获取角色列表接口
export const roleListAll = () => request.get('role/listAll')
// 创建获取指定角色权限的接口
export const adminRole = (id:number|undefined) => request.get('admin/role/'+id)
// 创建修改指定角色权限的接口
export const adminRoleUpdate = (data:FormData) => request.post('admin/role/update',data)
// 创建添加角色接口
export const adminRegister = (data:roleDetailData) => request.post('admin/register',data)
// 创建编辑角色接口
export const adminUpdate = (data:adminListDataType) => request.post('admin/update/'+data.id, data)