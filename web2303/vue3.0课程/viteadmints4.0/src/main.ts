import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";
// 引入阿里图标文件
import './font_/iconfont.css';
// 引入页面进度条的样式
import 'nprogress/nprogress.css';
// 导出router
import router from './router'
// 引入elementplus的 转中文的js
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
// 创建vue
const app = createApp(App);
// 全局加载
// 挂载ElemenePlus 组件
app.use(ElementPlus, {
    locale: zhCn,
  });
// 挂载路由
app.use(router)

app.mount("#app");
