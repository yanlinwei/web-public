// 导出创建路由实例
import { createRouter,createWebHashHistory } from "vue-router";

// 创建路由信息
const routes:any = [
    {
        // 地址
        path: '/',
        // 名称
        name: 'home',
        // 路由的组件
        component: () => import('@/views/templateVue/index.vue'),
        // 路由元信息
        meta: {
            isLogin: false,
            title: null
        }
    }
]

// 创建路由实例
const router = createRouter({
    routes,
    history: createWebHashHistory()
})

// 导出router实例
export default router
