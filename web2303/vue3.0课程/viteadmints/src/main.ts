import { createApp } from "vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";
// 导出router
import router from './router'
// 创建vue
const app = createApp(App);
// 全局加载
// 挂载ElemenePlus 组件
app.use(ElementPlus);
// 挂载路由
app.use(router)
app.mount("#app");
