// 引入axios
import axios from 'axios';

// 创建axios
const instance = axios.create({
    baseURL: 'https://admin-api.macrozheng.com/',
    timeout: 5000
})

// 请求拦截
instance.interceptors.request.use((config) => {
    return config
})

// 响应拦截
instance.interceptors.response.use((data) => {
    return data
})

// 导出
export default instance