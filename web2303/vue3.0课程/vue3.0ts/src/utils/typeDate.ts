export interface todoListData {
    id: number,
    title: string,
    checked: boolean
}