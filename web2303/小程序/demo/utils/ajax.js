let ajax = async function (url){
  // Promise

  /* 
    Promise 解决了回调地狱(调用的函数次数非常多)的问题 

  */
  return await new Promise(function (reslove,reject){
    
    wx.request({
      // 请求地址
      url,
      // 数据放在data里面 无论是 get 还是 post
      data:{
        username: 'admin',
        password: 'macro123'
      },
      // 请求头
      header: {
        // 异步获取 本地存储的信息
       token: wx.getStorageInfoSync('token') 
      },
      // 请求超时时间 ，默认 60s
      timeout: 5000,
      // 请求方式 POST  GET
      method: 'POST',
      // // 请求成功回调的函数
      success(res){
        // console.log(res);
        // promise 触发的then回调函数
        reslove(res)
      },
      // 请求失败的回调函数
      fail(err){
        // console.log(err);
        // reject 触发 catch的 回调函数
        reject(err)
      },
      // // 无论成功还是失败 都会回调的函数
      // complete(plete){
      //   // console.log(plete);
      // }
    })
  })
}
  
/* 
  写入 本地存储
  wx.setStorageSync('key', data)
*/

// 模块化导出
module.exports = {
  ajax
}