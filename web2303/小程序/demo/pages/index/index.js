// 导出 配置好的 ajax
const {ajax} = require('../../utils/ajax')
Page({
  // 页面加载中
  async onLoad(option){
    // option 参数 指的是上一个页面传递进来的参数
    console.log(option);
    // ajax('https://admin-api.macrozheng.com/admin/login').then(res => {
    //   console.log(res);
    // }).catch(err => {
    //   console.log(err);
    // })
    // async 和 await 是 把promise 转成 同步的写法 的修饰符 ES 7 语法

    let hasdu = await ajax('https://admin-api.macrozheng.com/admin/login');
    console.log(hasdu);
  },
  data: {
    /* 
      和vue 不一样的是 data不再是一个函数
      是一个对象 
    */
    text: "这是我的第一个微信小程序",
    className: 'sixsixsix',
    index: 2,
    arr: [
      '刘鹏',
      '杜朴',
      '韦玲玲',
      '王明珠',
      '张家南',
      '孙旭'
    ],
    markers: [{
      id: 1,
      latitude: '34.8003',
      longitude: '113.661',
      title: "金水区",
      iconPath: "/static/swiper/1.webp",
      width: 10
    }]
  },
  // 当前页面每显示一次都会出发的 生命周期
  onShow(){
    console.log('页面');
  },
  // 在相应的js当中 不需要写在methods里面 直接定义相对应的方法
  tap(){
    // 微信小程序的自带路由跳转方法
    wx.navigateTo({
      // 填写 绝对路径(一定不能是 app.json里面配置的tabBar里面的路径)
      // url: '/pages/logs/logs',
      url: '/pages/children/children',
      // 页面跳转成功 回调的函数
      success(res){
        console.log('页面跳转成功',res);
      },
      // 页面跳转失败回调的函数
      fail(res){
        console.log('页面跳转失败',res);
        // 判断页面跳转失败的原因 是不是因为 跳转的页面是 tabBar的页面
        if(res.errMsg.indexOf('tabbar') != -1){
          wx.switchTab({
            url: '/pages/logs/logs',
          })
        }
      }
    })
    // 如果要跳转导航页面，用另外一个API switchTab 跳转 tabBar页面
    // wx.switchTab({
    //   url: 'url',
    // })
  },
  gotap(){
    wx.navigateTo({
      url: '/pages/swiper/swiper?index=66',
    })
  },
  onPullDownRefresh(){
    console.log('用户触发了下拉刷新动作');
  }
})
