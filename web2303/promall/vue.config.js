module.exports = {
    // 代理
    devServer: {
        // 是否在项目初始化运行的时候 自动在浏览器打开网址 默认 false
        open: false,
        // 代理解决跨域
        proxy: {
            // '什么地址的前缀才使用跨域'
            '/webapi':{
                // 请求地址改变成哪个地址
                target: 'http://192.168.14.139/',
                // 是否需要跨域
                changeOrigin: true,
                // 是否是https访问
                secure: false,
                // 重写接口
                pathRewrite: {
                    // 重写请求
                    '^/webapi': '/'
                },
                // 做ip的骗术，告诉后端请求的地址来自官网
                onProxyReq(req){
                    // 骗服务器，来请求的页面是 服务器
                    req.setHeader("referer","http://192.168.14.139/")
                }
            },
            '/common': {
                // 请求地址改变成哪个地址
                target: 'https://api.chadian.com/',
                // 是否需要跨域
                changeOrigin: true,
                // 是否是https访问
                secure: true,
                // 重写接口
                pathRewrite: {
                    // 重写请求
                    '^/webapi': '/'
                },
                // 做ip的骗术，告诉后端请求的地址来自官网
                onProxyReq(req){
                    // 骗服务器，来请求的页面是 服务器
                    req.setHeader("referer","https://m.chadian.com/")
                }
            },
            '/geshou': {
                // 请求地址改变成哪个地址
                target: 'https://www.geshow.com/',
                // 是否需要跨域
                changeOrigin: true,
                // 是否是https访问
                secure: true,
                // 重写接口
                pathRewrite: {
                    // 重写请求
                    '^/geshou': '/'
                },
                // 做ip的骗术，告诉后端请求的地址来自官网
                onProxyReq(req){
                    // 骗服务器，来请求的页面是 服务器
                    req.setHeader("referer","https://www.geshow.com/")
                }
            },
            '/jiaju': {
                // 请求地址改变成哪个地址
                target: 'https://ss.absrclub.com/',
                // 是否需要跨域
                changeOrigin: true,
                // 是否是https访问
                secure: true,
                // 重写接口
                pathRewrite: {
                    // 重写请求
                    '^/jiaju': '/'
                },
                // 做ip的骗术，告诉后端请求的地址来自官网
                onProxyReq(req){
                    // 骗服务器，来请求的页面是 服务器
                    req.setHeader("referer","https://ss.absrclub.com/pages/index/index")
                }
            },
            '/aimu': {
                // 请求地址改变成哪个地址
                target: 'https://m.aimer.com.cn/',
                // 是否需要跨域
                changeOrigin: true,
                // 是否是https访问
                secure: true,
                // 重写接口
                pathRewrite: {
                    // 重写请求
                    '^/aimu': '/'
                },
                // 做ip的骗术，告诉后端请求的地址来自官网
                onProxyReq(req){
                    // 骗服务器，来请求的页面是 服务器
                    req.setHeader("referer","https://m.aimer.com.cn/")
                }
            }
        }
    }
    // devServer: {
    //     open: true, //是否启动项目 自动在默认浏览器打开
    //     proxy: {
    //         // /api 是接口的 字段
    //         '/api': {
    //             // 改变/api字段接口的请求地址
    //             target: 'http://192.168.14.139/',//目标接口域名
    //             changeOrigin: true,//是否跨域
    //             secure: false, //false为http访问，true为https访问
    //             pathRewrite: {
    //                 // 把所有的接口带有/api的字段 劫持替换成/
    //                 '^/api': '/' //重写接口
    //             },
    //             // 请求的拦截
    //             onProxyReq(req){
    //                 // 骗服务器，来请求的页面是 服务器
    //                 req.setHeader("referer","http://192.168.14.139/")
    //             }
    //         }
    //     }
    // }
}