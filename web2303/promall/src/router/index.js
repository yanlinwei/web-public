// 引入vue
import Vue from "vue";
// 引入vue-router
import VueRouter from "vue-router";
// 引入vuex
import store from '@/store'
// 注册vuerouter组件/插件
Vue.use(VueRouter);

// 创建路由信息
const routes = [
    {
        // 路由地址
        path: '/',
        // 路由名称
        name: '/',
        // 视图模板
        component: () => import('@/views/tabTemplate/index.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: false,
            // 标签页的标题
            title: '视图模板'
        },
        // 子路由嵌套
        children: [
            {
                // 路由地址
                path: '/',
                // 路由名称
                name: '/',
                // 视图模板
                component: () => import('@/views/home/index.vue'),
                // 路由元信息
                meta: {
                    // 是否需要登录
                    isLogin: false,
                    // 标签页的标题
                    title: '首页'
                }
            },
            {
                // 路由地址
                path: '/community',
                // 路由名称
                name: 'community',
                // 视图模板
                component: () => import('@/views/community/index.vue'),
                // 路由元信息
                meta: {
                    // 是否需要登录
                    isLogin: false,
                    // 标签页的标题
                    title: '社区'
                }
            },
            {
                // 路由地址
                path: '/cart',
                // 路由名称
                name: 'cart',
                // 视图模板
                component: () => import('@/views/cart/index.vue'),
                // 路由元信息
                meta: {
                    // 是否需要登录
                    isLogin: true,
                    // 标签页的标题
                    title: '购物车'
                }
            },
            {
                // 路由地址
                path: '/user',
                // 路由名称
                name: 'user',
                // 视图模板
                component: () => import('@/views/user/index.vue'),
                // 路由元信息
                meta: {
                    // 是否需要登录
                    isLogin: true,
                    // 标签页的标题
                    title: '个人中心'
                }
            }
        ]
    },
    {
        // 路由地址
        path: '/login',
        // 路由名称
        name: 'login',
        // 视图模板
        component: () => import('@/views/user/login.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: false,
            // 标签页的标题
            title: '登录'
        }
    },
    {
        // 路由地址
        path: '/register',
        // 路由名称
        name: 'register',
        // 视图模板
        component: () => import('@/views/user/register.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: false,
            // 标签页的标题
            title: '注册'
        }
    },
    {
        // 路由地址
        path: '/setup',
        // 路由名称
        name: 'setup',
        // 视图模板
        component: () => import('@/views/user/setup/index.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: true,
            // 标签页的标题
            title: '设置'
        }
    },
    {
        // 路由地址
        path: '/edituser',
        // 路由名称
        name: 'edituser',
        // 视图模板
        component: () => import('@/views/user/setup/edit.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: true,
            // 标签页的标题
            title: '修改资料'
        }
    },
    {
        // 路由地址
        path: '/editpas',
        // 路由名称
        name: 'editpas',
        // 视图模板
        component: () => import('@/views/user/setup/editPassword.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: true,
            // 标签页的标题
            title: '修改密码'
        }
    },
    {
        // 路由地址
        path: '/address',
        // 路由名称
        name: 'address',
        // 视图模板
        component: () => import('@/views/user/setup/address/index.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: true,
            // 标签页的标题
            title: '收货地址列表'
        }
    },
    {
        // 路由地址
        path: '/addaddress',
        // 路由名称
        name: 'addaddress',
        // 视图模板
        component: () => import('@/views/user/setup/address/add.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: true,
            // 标签页的标题
            title: '新增收货地址'
        }
    },
    {
        // 路由地址 动态路由接收address_id参数
        path: '/editAddress/:address_id',
        // 路由名称
        name: 'editAddress',
        // 视图模板
        component: () => import('@/views/user/setup/address/edit.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: true,
            // 标签页的标题
            title: '修改收货地址'
        }
    },
    {
        // 路由地址
        path: '/search',
        // 路由名称
        name: 'search',
        // 视图模板
        component: () => import('@/views/home/search.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: false,
            // 标签页的标题
            title: '搜索商品'
        }
    },
    {
        // 路由地址 多个参数 动态路由匹配方式 
        // 只有在product_id和admin_id参数同时传入时 页面才会显示|
        path: '/productDetail/:product_id/:admin_id',
        // 路由名称
        name: 'productDetail',
        // 视图模板
        component: () => import('@/views/product/index.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: false,
            // 标签页的标题
            title: '商品详情'
        }
    }
];

// 创建路由实例
const router  = new VueRouter({
    routes
})

// 路由前置导航守卫
router.beforeEach((go,come,next) => {
    // 改变标签的标题
    document.title = go.meta.title;
    // 判断是否需要登录
    if(go.meta.isLogin){
        // 是否登录了
        if(store.state.token){
            next();
        }else{
            // console.log(go,come);
            next({
                path: '/login',
                // query把信息传递给要跳转的页面
                query: {
                    path: go.path
                }
            });
        }
    }else{
        next();
    }
})

// 导出路由实例
export default router