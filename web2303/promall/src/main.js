import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 导入vant组件库
import Vant from 'vant';
// 导入vant组件库对应的css样式
import 'vant/lib/index.css';
// 导入自己写的输入框组件
import pInput from '@/components/pInput.vue'

// 导入自定义导航组件
import pNavigator from '@/components/pNavgaitor.vue'

// 导入vant指令 懒加载
import { Lazyload } from 'vant';

// 引入配置好的vuex
import store from '@/store'
// 注册懒加载的指令
Vue.use(Lazyload);
// 在vue全局注册自己写的输入框组件
Vue.component('p-input',pInput)
// 在vue全局注册自己写的导航组件
Vue.component('p-navigator',pNavigator)
// 在当前Vue中使用vant组件
Vue.use(Vant)
Vue.config.productionTip = false

new Vue({
  router,
  // 注册vuex状态
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
