/* 
    混入对象mixin 拥有和 Vue实例 一模一样的 实例对象参数
    相当于，拥有Vue生命周期，计算属性，监听，data数据等。
    当vue实例 使用了 mixin混入对象，mixin混入对象会比
    vue实例 要快;
*/
// 引入获取验证码的接口
import {getCode} from '@/api'
const myMiXin = {
    data() {
        return {
           // 验证码数据
           ercodes: {
            text: '',
            data: ''
        },
        }
    },
    created() {
        this.getCodeFun();
        console.log(123);
    },
    methods: {
        getCodeFun(){
            getCode({width: 120,height: 40}).then(res => this.ercodes = res.data)
        }
    },
}

// 定义好的mixin 导出
export default myMiXin