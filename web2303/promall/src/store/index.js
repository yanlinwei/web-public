// 引入 vue
import Vue from 'vue';
// 引入 vuex
import VueX from 'vuex';
// 引入vuex数据持久化插件
import persist from 'vuex-persist'

// 设置数据持久化的方式
const storege = new persist({
    // 存储方式：  本地存储
    storage: window.localStorage
})
// 在 vue 中使用 vuex
Vue.use(VueX);

// 创建 store 仓库
const store = new VueX.Store({
    // 用于存储 数据
    state: {
        // 用户的登录凭证
        token: null,
        // 用户信息
        userInfo:{
            headImg: require('@/assets/logo.jpg'),
            nickName: '昵称',
            desction: '简介',
            price: 0,
            polarBean: 0,
            fans: 0,
            follow: 0
        }
    },
    // 修改state的状态
    mutations: {
        changeState(state,token){
            /* 
               state,是必须参数，是传入的state的库
                token 是修改时传递的参数
            */
            //    直接修改state的 值
            state.token = token;
        },
        changeUserInfo(state,userInfo){
            /* 
               state,是必须参数，是传入的state的库
                token 是修改时传递的参数
            */
            //    直接修改state的 值
            state.userInfo = userInfo;
        }
    },
    // 使用vuex 数据持久化操作
    plugins: [storege.plugin]
})

// 导出
export default store