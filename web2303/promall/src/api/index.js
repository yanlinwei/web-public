// 导入配置好的axios
import axios from './axios.js'

// 创建获取验证码接口
export const getCode = (data) => axios.post('webapi/getCode',data)
// 用户注册接口
export const register = (data) => axios.post('register',data)
// 用户登录接口
export const login = (data) => axios.post('webapi/login',data)
// 获取用户信息接口
export const getUserInfo = () => axios.post('webapi/getUserInfo')
// 获取手机验证码接口
export const sendCodeMsg = (params) => axios.get('webapi/sendCodeMsg',{params})
// 手机验证码登录
export const verifyCodeMsg = (params) => axios.get('webapi/verifyCodeMsg',{params})
// 退出登录
export const loginOut = () => axios.post('webapi/loginOut')
// 上传文件接口
export const upload = (data) => axios.post('webapi/upload',data)
// 修改资料接口
export const editUserInfo = (data) => axios.post('webapi/editUserInfo',data)
// 修改密码接口
export const forgetPassword = (data) => axios.post('webapi/forgetPassword',data)
// 新增收货地址
export const addAddress = (data) => axios.post('webapi/addAddress',data)
// 获取收获地址列表
export const getAddressList = (data) => axios.post('webapi/getAddressList',data)
// 修改收货地址信息
export const editAddress = (data) => axios.post('webapi/editAddress',data)
// 获取地址详情
export const getAddressDesc = (data) => axios.post('webapi/getAddressDesc',data)
// 删除收货地址
export const deleteAddressDesc = (data) => axios.post('webapi/deleteAddressDesc',data)
// 获取首页数据接口
export const getHome = (params) => axios.get('webapi/getHome',{params})
// 获取商品数据
export const getProductList = (params) => axios.get('webapi/getProductList',{params})
// 获取商品详情getProductDesc
export const getProductDesc = (params) => axios.get('webapi/getProductDesc',{params})
// 加入购物车接口addCartList
export const addCartList = (data) => axios.post('webapi/addCartList',data)
// 获取购物车列表接口
export const getCartList = () => axios.get('webapi/getCartList')
// 删除购物车
export const delCartNum = (data) => axios.post('webapi/delCartNum',data)
// 修改购物车数量
export const editCartNum = (data) => axios.post('webapi/editCartNum',data)