// 配置axios
// 导入axios
import axios from "axios";
// 引入vuex
import store from '@/store'
// 创建axios的实例
const instance = axios.create({
    // baseURL 请求的地址
    // baseURL: 'http://192.168.14.139/',
    baseURL: '/',
    // 请求超时时间(ms)
    timeout: 15000
})

// 请求头配置，需要在每次发送请求的时候，给请求加上请求头部
// 在发送请求之前做一次请求拦截
instance.interceptors.request.use((config) => {
    // 配置请求头部 token
    config.headers.token = store.state.token;
    config.headers.Version = 'h5_v1';
    config.headers['Form-Type'] = 'h5';
    // 需要把配置好的参数返回（return），如果不返回 请求就不会发送
    return config
})

// 后端响应拦截
instance.interceptors.response.use((data) => {
    // 拿到数据 数据转换成字符串
    data.data = JSON.stringify(data.data)
    // 使用字符串替换方法
    data.data = data.data.replace(/192.168.17.126/g,'192.168.14.139').replace(/www.poralmall.com/g,'192.168.14.139').replace(/192.168.13.10/g,'192.168.14.139');
    // 再重新转回对象格式
    data.data = JSON.parse(data.data)
    // 需要把处理好的data数据返回出去
    return data
})

// 导出axios
export default instance