import Vue from 'vue'
// import App from './App.vue'
// import App from './soltCompont.vue'
import App from './component.vue'
// 导入todoList
// import todoListVue from './components/homework/todoList.vue'
// 全局注册组件
// Vue.component('todoList',todoListVue)
Vue.config.productionTip = false

new Vue({
  /* 
    渲染vue模板的函数,
    h(App),以名字为App的组件作为渲染模板
  */
  render: function (h) { return h(App) },
}).$mount('#app')
