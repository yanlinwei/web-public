import Vue from 'vue'
import App from './App.vue'
// 导入配置好的vue-router文件
import router from './router/index'
Vue.config.productionTip = false

new Vue({
  // 在vue实例当中使用 router
  router,
  render: function (h) { return h(App) },
}).$mount('#app')
