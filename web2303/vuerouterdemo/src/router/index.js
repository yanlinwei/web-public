/* 
    vue-router路由(地址)配置
*/
// 1.导入Vue实例对象
import Vue from 'vue';
// 2.导入vue-router实例对象
import VueRouter from 'vue-router';
// 3.在vue实例中使用VueRouter组件
Vue.use(VueRouter);
// 引入首页的路由视图
/* 
    导入如果不使用，
    会浪费性能；
    如何优化，当匹配到哪个路由的时候，
    就加载哪个路由视图(路由懒加载);
*/
// import Home from '@/views/home.vue'
// import Catgary from '@/views/cart.vue'
// import Community from '@/views/community.vue'
// import Myself from '@/views/myself.vue'
// 4. 配置vue-router (路由)参数
const routes = [
    {
        // 1.路由地址
        path: '/',
        // 路由名称
        name: 'home',
        // 路由的别名
        alias: '/home',
        /* 
            别名的意义：
                当访问 地址为 / 和 /home的页面时，
                访问的时同一个视图,
        */
        // 视图的模板  路由懒加载
        component: () => import('@/views/home.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: false,
            // 当前页面的标签title
            title: '首页'
        }
    },
    {
        // 1.路由地址
        path: '/catgary',
        // 路由名称
        name: 'catgary',
        // 重定向
        /* 
            访问一个地址，重新修改 路由地址
            到指定的匹配路由视图上
        */
        redirect: '/cart',
        // 视图的模板
        component: () => import('@/views/cart.vue')
    },
    {
        // 1.路由地址
        path: '/cart',
        // 路由名称
        name: 'cart',
        // 视图的模板
        component: () => import('@/views/cart.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: true,
            // 当前页面的标签title
            title: '购物车'
        }
    },
    {
        // 1.路由地址
        path: '/community',
        // 路由名称
        name: 'community',
        // 视图的模板
        component: () => import('@/views/community.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: false,
            // 当前页面的标签title
            title: '社区'
        }
    },
    {
        // 1.路由地址
        path: '/myself',
        // 路由名称
        name: 'myself',
        // 视图的模板
        component: () => import('@/views/myself.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: true,
            // 当前页面的标签title
            title: '个人中心'
        }
    },
    {
        // 1.路由地址
        path: '/login',
        // 路由名称
        name: 'login',
        // 视图的模板
        component: () => import('@/views/login.vue'),
        // 路由元信息
        meta: {
            // 是否需要登录
            isLogin: false,
            // 当前页面的标签title
            title: '登录'
        }
    }
]
// 5. 创建vue-router实例
const router = new VueRouter({
    routes,
    // 路由模式 hash 默认  history
    mode: 'hash'
    // mode: 'history'
    /* 
        hash模式 和history 模式的区别:
            hash没history好看，
            在加载过程当中，hash有页面缓存，
            history没有页面缓存，
            部署项目的时候，hash不需要服务器配置，
            history需要服务端配置
    */
})

// 在进入下一个路由之前进行一个判断，逻辑处理 (前置导航守卫)
router.beforeEach((to,from,next) => {
    /* 
        to 要前往的路由地址信息
        from 上一个路由地址信息
        next(option); 如果不写next()函数，会导致导航阻塞
            option 为 null 正确前往下一站
            option 为 false 取消当前导航
            option 为 path地址 去往path的路由地址

    */
    // 改变页面标签的标题
    document.title = to.meta.title;
    // 把本地存储的 token 当作 是否登录的依据
    // 先判断 要跳转的页面需不需要登录
    if(to.meta.isLogin){
        // 在判断有没有登录
        if(localStorage.getItem('token')){
            next();
        }else{
            next('/login')
        }
    }else{
        next();
    }
})
// 导出vue-router实例
export default router