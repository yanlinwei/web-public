import Vue from 'vue'
// import 是 script 的 type="module" 模式下的引用方式
// import 变量名 from 'url' url是相对路径
// 如果在本地文件夹找不到引入的文件 
// 会去node_modules里面去找
// 一般来说 本地路径的文件 一般都要指定./路径的标准写法
// 依赖包的文件 不用指定路径
// import App from './App.vue'
//import Box from './Box.vue'
// import Come from './Come.vue'
import Games from './Games.vue'


// 去除开发环境的警示
Vue.config.productionTip = false

new Vue({
  // 渲染App.vue文件专属方法
  // render: function (h) { return h(App) },
  //render: function (h) { return h(Box) },
  // render: function (h) { return h(Come) },
  render: function (h) { return h(Games) },
}).$mount('#app') // 挂载dom
