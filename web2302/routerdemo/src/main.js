import Vue from 'vue'
// import App from './App.vue'

import RouterVue from './Router.vue'

// 引入配置好的router文件
import router from './router/router.js'
// 引入配置好的router文件
// import router from './router'
// 如果文件不指定 具体的
// 会自动匹配 文件夹下的 index.js或者 index.vue
Vue.config.productionTip = false

new Vue({
  // 全局注册vue-router配置
  router,
  // 所有的组件 都会编译到App.vue运行
  // render: function (h) { return h(App) },
  render: function (h) { return h(RouterVue) },
}).$mount('#app')
