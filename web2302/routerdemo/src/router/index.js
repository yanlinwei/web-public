// 1. 引入vue-router
import vueRouter from 'vue-router';
// 引入Vue
import Vue from 'vue';
// 再vue实例中使用vue-router
// 注册了vue-router的专属组件
Vue.use(vueRouter)
// 引入login.vue视图
import loginVue from '@/pages/login.vue';
// 引入home.vue视图
import homeVue from '@/pages/home.vue';
// 引入新闻视图
import newsVue from '@/pages/news.vue'
// 引入用户视图
import userVue from '@/pages/user.vue';
// 引入404视图
import notFound from '@/pages/404.vue'
// 2. 配置路由
const routes = [
    {
        // 路径
        // '/' 代表根目录
        path: '/',
        // 路由起名字
        name:'home',
        // 路由的重定向
        // redirect:{
        //     // 跳转当前路由 会被指向于 name=news这个路由
        //     name:'news'
        // },
        // 别名
        alias:'/home',
        // 路由的名字
        component: homeVue
    },
    // {
    //     // 路径
    //     // '/' 代表根目录
    //     path: '/login',
    //     // 路由起名字
    //     name:'login',
    //     // 路由的名字
    //     component: userVue
    // },
    {
        // 路径
        // '/' 代表根目录
        path: '/login',
        // 路由起名字
        name:'login',
        // 路由的名字
        component: loginVue
    },
    {
        // 路径
        // '/' 代表根目录
        path: '/news',
        // 路由起名字
        name:'news',
        // 路由的名字
        component: newsVue
    },
    {
        // 匹配路由
        // /user/匹配的id
        path: '/user/:user_id',
        name: 'user',
        component: userVue
    },
    {
        // 尽量把404页面配置在最后方
        // 因为路由的优先级问题
        // 无论什么地址都会跳转这个路由(404页面)
        path: '*',
        // 路由
        component: notFound
    }
]

// 3. 创建vue-router实例
const router = new vueRouter({
    // 把配置好的路由放进去
    // routes: routes
    routes
})
// 返回router实例
export default router