// 引入vue-router
import VueRouter from "vue-router";
// 引入vue
import Vue from "vue";
// 在vue中使用vue-router
Vue.use(VueRouter);
// import tempRouter from '@/routerPage/tempRouter.vue'
// 配置路由
const routes = [
    {
        // 路由地址
        path: '/',
        // 引入路由
        // component: tempRouter
        // 使用路由懒加载(推荐使用)
        // 只有跳转到此路由才会加载路由视图
        component: () => import('@/routerPage/tempRouter.vue'),
        // 子路由
        children: [
            {
                // 子路由的地址 
                // 访问方式 
                // 父路由的地址 + 子路由的地址
                // 如果子路由 开头是/不适合上述规则
                path: 'child1',
                // 子路由的视图
                component: () => import('@/routerPage/children/child1.vue')
            },
            {
                // 子路由的地址 
                // 访问方式 
                // 父路由的地址 + 子路由的地址
                // 如果子路由 开头是/不适合上述规则
                path: '/sixsixsix',
                // 子路由的视图
                component: () => import('@/routerPage/children/child2.vue')
            }
        ]
    }
]

// vue-router实例
const router = new VueRouter({
    routes
})

// 导出
export default router