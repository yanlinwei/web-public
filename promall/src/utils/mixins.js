import {
    // 引入 获取用户信息接口
    getUserInfo
} from '@/api'
// 引入store
import store from '@/store'
// 创建mixin对象
const app = {
    data() {
        return {
            userInfo: {
                desction: '',
                fans: 0,
                follow: 0,
                headImg: '',
                loginPhone: 0,
                nickName: '',
                polarBean: 0,
                price: 0,
                sex: 0
            },
            index: 0
        }
    },
    // mixin 的 生命周期
    created() {
        // 调用methods接口
        this.getUser();
        // console.log('minxins');
        // 在mixins 也可以调用 vue实例的方法
        console.log(this.title);
    },
    methods: {
        getUser(){
            // 获取用户信息
            getUserInfo().then(res => {
                // 判断有没有请求成功
                if(res.data.code == 0){
                    // 弹出vant的轻提示
                    this.$toast({
                        message: res.data.msg,
                        duration: 1500,
                        onClose: () => {
                            // 先清空本地存储的信息
                            localStorage.clear();
                            this.$router.push('/login');
                        }
                    })
                }else{
                    // 把本地的数据 替换成服务器返回的数据
                    this.userInfo = res.data.result;
                    // 把用户信息存储在本地
                    // localStorage.setItem('userInfo',JSON.stringify(res.data.result))
                    store.commit('changeUser',res.data.result)
                }
            })
        }
    },
}


// 导出
export default app