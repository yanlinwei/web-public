// 引入vue-router
import VueRouter from 'vue-router';
// 引入Vue
import Vue from 'vue';
// 引入Vant组件的轻提示插件
import { Toast } from 'vant';
// 把vuex的状态引入
import store from '@/store'
// 使用vuerouter组件
Vue.use(VueRouter);
// 配置路由
const routes = [
    {
        // 地址
        path: '/',
        // 路由懒加载
        component: () => import('@/pages/tabTemp.vue'),
        // 重定向
        redirect: {
            path: '/home'
        },
        // 子路由
        children: [
            {
                // 子路由地址
                path: '/home',
                // 路由懒加载
                component: () => import('@/pages/home/index.vue'),
                // 路由的元信息
                meta: {
                    // 视图的标题
                    title: '首页',
                    // 验证登录
                    isLogin: false
                }
            },
            {
                // 子路由地址
                path: '/community',
                // 路由懒加载
                component: () => import('@/pages/community/index.vue'),
                // 路由的元信息
                meta: {
                    // 视图的标题
                    title: '社区',
                    // 验证登录
                    isLogin: false
                }
            },
            {
                // 子路由地址
                path: '/cart',
                // 路由懒加载
                component: () => import('@/pages/cart/index.vue'),
                // 路由的元信息
                meta: {
                    // 视图的标题
                    title: '购物车',
                    // 验证登录
                    isLogin: true
                }
            },
            {
                // 子路由地址
                path: '/myself',
                // 路由懒加载
                component: () => import('@/pages/myself/index.vue'),
                // 路由的元信息
                meta: {
                    // 视图的标题
                    title: '我的',
                    // 验证登录
                    isLogin: true
                }
            }
        ]
    },
    {
        // 路由地址
        path: '/login',
        // 路由懒加载
        component: () => import('@/pages/myself/login.vue'),
        // 路由元信息
        meta: {
            title: '登录',
            isLogin: false
        }
    },
    {
        // 路由地址
        path: '/register',
        // 路由懒加载
        component: () => import('@/pages/myself/register.vue'),
        // 路由元信息
        meta: {
            title: '注册',
            isLogin: false
        }
    },
    {
        // 路由地址
        path: '/setup',
        // 路由懒加载
        component: () => import('@/pages/myself/setup/index.vue'),
        // 路由元信息
        meta: {
            title: '设置',
            isLogin: true
        }
    },
    {
        // 路由地址
        path: '/editPas',
        // 路由懒加载
        component: () => import('@/pages/myself/setup/editPassword.vue'),
        // 路由元信息
        meta: {
            title: '修改密码',
            isLogin: true
        }
    },
    {
        // 路由地址
        path: '/edituser',
        // 路由懒加载
        component: () => import('@/pages/myself/setup/edit.vue'),
        // 路由元信息
        meta: {
            title: '修改资料',
            isLogin: true
        }
    },
    {
        // 路由地址
        path: '/address',
        // 路由懒加载
        component: () => import('@/pages/myself/setup/address/index.vue'),
        // 路由元信息
        meta: {
            title: '收货地址',
            isLogin: true
        }
    },
    {
        // 路由地址
        path: '/addressadd',
        // 路由懒加载
        component: () => import('@/pages/myself/setup/address/add.vue'),
        // 路由元信息
        meta: {
            title: '新增收货地址',
            isLogin: true
        }
    },
    {
        // 路由地址
        path: '/editAddress/:address_id',
        // path: '/editAddress',
        // 路由懒加载
        component: () => import('@/pages/myself/setup/address/edit.vue'),
        // 路由元信息
        meta: {
            title: '修改收货地址',
            isLogin: true
        }
    },
    {
        // 路由地址
        path: '/suggest',
        // 路由懒加载
        component: () => import('@/pages/myself/setup/suggest.vue'),
        // 路由元信息
        meta: {
            title: '给极点建议',
            isLogin: true
        }
    },
    {
        // 路由地址
        path: '/product_detail',
        // 路由懒加载
        component: () => import('@/pages/home/product/detail.vue'),
        // 路由元信息
        meta: {
            title: '商品详情',
            isLogin: true
        }
    },
    {
        // 路由地址
        path: '/search',
        // 路由懒加载
        component: () => import('@/pages/home/product/search.vue'),
        // 路由元信息
        meta: {
            title: '商品搜索',
            isLogin: true
        }
    }
];

// 创建vue-router实例
const router = new VueRouter({
    // mode : 'hash'(默认)  'history'
    // mode: 'hash',
    // mode:'history',
    /* 
        关于history 服务端的配置问题
        如果项目开发完成之后，打包（npm run build）配置到服务器的时候，
        hash模式，直接放在服务端即可，不需要做额外的配置；
        history模式，需要服务端配置，配置好之后，才可以正确访问页面；
        具体原因是，不带有 #标识的地址，会被默认为get请求;
    */
    routes
})

// 跳转前的判断
// 全局导航守卫
// 全局的前置导航守卫
router.beforeEach((to,from,next) => {
    // 判断要跳转的路由 是否需要登录
    if(to.meta.isLogin){
        // 判断有没有登录
        if(store.state.token){
            // 页面的标题
            document.title = to.meta.title;
            next()
        }else{
            // 跳转到登录页面
            // next('/login')
            Toast.loading({
                message: '请先登录',
                // vant组件的展示时长 (ms)
                duration: 1500,
                // 关闭后的回调函数
                onClose: function (){
                    if(from.path == '/login'){
                        // next(false) 阻止路由跳转
                        next(false)
                    }else{
                        router.push('/login')
                    }
                }
            });
        }
    }else{
        // 因为VUE是单页应用 
        // 所以说跳转的时候直接改变title就可以改变
        // 页面的标题
        document.title = to.meta.title;
        next()
    }
    // console.log(to);
    /* 
        to 
            是要跳转的路由信息
    */
   
    // console.log(from);
    /* 
        from 
            是从哪个路由跳转来的路由信息
    */
    
    /* 
        next 
            是一个函数，
            如果不调用就不会跳转路由
        next()
            小括号里面不写任何参数，证明
            正常跳转

            如果写了一个路由的地址
            就会跳转到 相应的路由
    */
    
})

// 导出router实例
export default router