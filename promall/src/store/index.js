/* 
    一般来说，在我们项目开发的过程中，
    需要将多个页面应用到的 相同的数据，
    统一管理，经过统一的方法，将这些数据
    统一处理。
    比如：用户的登录状态，用户信息等数据存放在一起。

    我们可以自己写一些管理的方法，也可以使用别的开发者
    给我们开发好的工具，比如vuex(vue2)\pinia(vue3);

    1. vuex的安装方式
        npm install vuex@3 --save
*/
//引入 vue
import Vue from 'vue';
// 引入vuex
import Vuex from 'vuex';
/* 
    安装 vuex 永久储存插件
    npm install vuex-persist
*/
// 引入vuex-persist插件
import xPersist from 'vuex-persist';
// 创建vuex-persist实例
const vuexLocal = new xPersist({
    // 存储方式 localStorage  sessionStorage cookie
    storage: window.localStorage
})
// 在vue上注册vuex 使之可以在vue上使用vuex的方法
Vue.use(Vuex);

// 创建vuex的仓库
const store = new Vuex.Store({
    // 状态
    state(){
        return {
            // 是否登录
            isLogin: false,
            // token
            token: '',
            // 用户信息
            userInfo: {
                desction: '',
                fans: 0,
                follow: 0,
                headImg: null,
                loginPhone: 0,
                nickName: '',
                polarBean: 0,
                price: 0,
                sex: 0
            }
        }
    },
    // 在mutations 创建函数修改 state里面的状态
    mutations: {
        // 改变token的事件
        changeToken(state,token){
            state.token = token;
        },
        // 改变用户信息的事件
        changeUser(state,userInfo){
            state.userInfo = userInfo;
        },
        // 形参 state
        // 是 上面状态中的所有的数据
        changeLogin(state,value){
            /* 
                value可选值，
                如果说没有具体要修改的参数，
                可以不传入value
                在当前的方法中直接修改state对象
            */
           console.log(value);
        //    console.log(state.isLogin);
            // 修改state对象里面的参数
            state.isLogin = true;
        }
    },
    plugins:[vuexLocal.plugin] 
})


// 导出vuex仓库
export default store