// 引入配置好的axios文件
import axios from './axios';
// 创建接口
// 获取验证码的接口
export const getCode = (data) => axios.post('getCode',data)

// 登录接口
export const Login = (data) => axios.post('login',data)

// 注册接口
export const Register = (data) => axios.post('register',data);

// 获取用户信息
export const getUserInfo = () => axios.post('getUserInfo');

// 退出登录
export const LoginOut = () => axios.post('loginOut');

// 修改密码
export const forgetPassword = (data) => axios.post('forgetPassword',data);

// 上传文件
export const uploadFile = (data) => axios.post('upload',data);

// 修改用户资料
export const editUser = (data) => axios.post('editUserInfo',data)

// 新增收货地址
export const addAddress = (data) => axios.post('addAddress',data)

// 获取收货地址列表
export const getAddressList = () => axios.post('getAddressList')

// 修改地址信息
export const editAddress = (data) => axios.post('editAddress',data)

// 获取地址详情
export const getAddressDesc = (data) => axios.post('getAddressDesc',data)

// 删除地址
export const deleteAddressDesc = (data) => axios.post('deleteAddressDesc',data)

// 意见建议
export const advise = (data) => axios.post('advise',data)

// 首页数据
export const getHome = (params) => axios.get('getHome',{params})

// 获取商品详情
export const getProductDesc = (params) => axios.get('getProductDesc',{params})

// 添加购物车
export const addCartList = (data) => axios.post('addCartList',data)

// 获取购物车列表
export const getCartList = () => axios.get('getCartList')

// 删除购物车
export const delCartNum = (data) => axios.post('delCartNum',data)

// 修改购物车数量
export const editCartNum = (data) => axios.post('editCartNum',data)

// 获取商品列表
export const getProductList = (params) => axios.get('getProductList',{params})