// 引入axios
import axios from 'axios';
// 引入vuex的状态
import store from '@/store'
// 创建axios实例
const instance = axios.create({
    // 请求地址
    // baseURL: 'http://192.168.13.10/',
    // 如果说baseURL 填写了带有http || https的地址
    // 代理是无法劫持请求的
    baseURL: '/api/',
    // 请求的超时时间 (ms)
    timeout: 5000
});

// 添加请求拦截器
instance.interceptors.request.use((config) => {
    // 每一次发送请求都会 触发请求拦截器
    // console.log(123);
    // config 形参是用于配置 发送请求时需要携带的参数的
    // console.log(config);
    // 配置请求头
    config.headers.token = store.state.token;
    return config
})

// 导出axios实例
export default instance