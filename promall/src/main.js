import Vue from 'vue'
import App from './App.vue'
// 引入配置好的router
import router from './router'
Vue.config.productionTip = false
// 从Vue实例的原型链上继承 title属性(全局变量)
Vue.prototype.$title = 'vue';
// 引入 Vant组件库
import Vant from 'vant';
// 引入Vant组件库搭配的样式
import 'vant/lib/index.css'
// 引入vant得 图片懒加载
import { Lazyload } from 'vant';

Vue.use(Lazyload);
// 注册组件
Vue.use(Vant);

// 全局指令注册
Vue.directive('focus',{
  inserted(element){
    if(element.nodeName === 'INPUT'){
      element.focus()
    }else{
      throw Error('focus only use input')
    }
  }
})

// 引入配置好的vuex文件
import store from '@/store'

new Vue({
  // 注册router
  router,
  // 注册vuex
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
