module.exports = {
    // 代理
    devServer: {
        open: true, //是否启动项目 自动在默认浏览器打开
        proxy: {
            // /api 是接口的 字段
            '/api': {
                // 改变/api字段接口的请求地址
                target: 'http://192.168.14.139/',//目标接口域名
                changeOrigin: true,//是否跨域
                secure: false, //false为http访问，true为https访问
                pathRewrite: {
                    // 把所有的接口带有/api的字段 劫持替换成/
                    '^/api': '/' //重写接口
                },
                // 请求的拦截
                onProxyReq(req){
                    // 骗服务器，来请求的页面是 服务器
                    req.setHeader("referer","http://192.168.14.139/")
                }
            }
        }
    }
}